module ApplicationHelper

  # Returns the full title on a per-page basis. Sorgt dafür. dass Die Seite ohne Strich bei home angezeigt wird
  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title # return sagt, dass er nur den base_title zurückgibt
    else
      page_title + " | " + base_title
    end
  end
end